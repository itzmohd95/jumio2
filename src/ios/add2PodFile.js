const fs = require("fs");
const path = require("path");

function add2PodFile(platformRoot) {

    const projectBuildFile = path.join(platformRoot, "Podfile");

    // let fileContents = fs.readFileSync(projectBuildFile, "utf8");

    // const myRegexp = /\bCordova\b.*/g;
    // let match = myRegexp.exec(fileContents);
    // if (match != null) {
    //     let insertLocation = match.index + match[0].length;

    //     fileContents =
    //         fileContents.substr(0, insertLocation) +
    //         "\n" +
    //         "source 'https://cdn.cocoapods.org'" +
    //         "\n" +
    //         fileContents.substr(insertLocation);

    //     fs.writeFileSync(projectBuildFile, fileContents, "utf8");

    //     console.log("Done");
    // } else {
    //     console.error("unable to insert");
    // }

    let newLine = "\n"+"post_install do |installer|\r\n  installer.pods_project.targets.each do |target|\r\n    if [\'iProov\', \'Socket.IO-Client-Swift\', \'Starscream\'].include? target.name\r\n      target.build_configurations.each do |config|\r\n          config.build_settings[\'BUILD_LIBRARY_FOR_DISTRIBUTION\'] = \'YES\'\r\n      end\r\n    end\r\n  end\r\nend";

    
    fs.appendFileSync(projectBuildFile, newLine, (err) => {
        if (err) {
          console.log(err);
        }
        else {
          // Get the file contents after the append operation
          console.log("\nDone");
        }
    })
}

module.exports = context => {
    "use strict";
    const platformRoot = path.join(context.opts.projectRoot, "platforms/ios");

    return new Promise((resolve, reject) => {
        add2PodFile(platformRoot);
        resolve();
    });
};